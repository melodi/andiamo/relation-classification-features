import argparse
import sys


def parse_args():
    """
    Parse input arguments.
    """
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--data_path",
        default="./data",
        type=str,
        help="The path to the shared task data file from Github.",
    )

    parser.add_argument(
        "--mappings_file",
        default="mappings/mappings_final.tsv",
        type=str,
        help="The mappings file for all relations.",
    )

    # transformer model
    parser.add_argument(
        "--transformer_model",
        default="bert-base-multilingual-cased",
        type=str,
        help="Model used, default: bert-multilingual-base-cased",
    )
    # tokenizer (you need to load the model tokenizer for checkpoint models)
    parser.add_argument(
        "--tokenizer_model",
        default="bert-base-multilingual-cased",
        type=str,
        help="Tokenizer, default: bert-multilingual-base-cased",
    )

    # Number of training epochs
    parser.add_argument(
        "--num_epochs",
        default=4,
        type=int,
        help="Number of training epochs. Default: 4",
    )

    # Number of gradient accumulation steps
    parser.add_argument(
        "--gradient_accumulation_steps",
        default=16,
        type=int,
        help="Number of gradient accumulation steps. Default: 16",
    )

    # Dropout
    parser.add_argument("--dropout", default=0.1, type=float, help="Dropout.")

    # Batch size
    parser.add_argument(
        "--batch_size",
        default=8,
        type=int,
        help="With CUDA: max. 8, without: max. 16. Default: 8",
    )

    # number_of_pos_tags
    parser.add_argument(
        "--number_of_pos_tags", default=2, type=int, help="number_of_pos_tags"
    )

    # Use CUDA
    parser.add_argument(
        "--use_cuda",
        default="yes",
        type=str,
        help="Use CUDA [yes/no]. Careful of batch size!",
    )

    # freeze layers
    parser.add_argument(
        "--freeze_layers",
        default="",
        type=str,
        help="List of layer(s) to freeze, a str separated by ;. Example: 'layer.1;layer.2'",
    )

    # normalize direction
    parser.add_argument(
        "--normalize_direction",
        default="no",
        type=str,
        help="How will the direction be handled. Options: discret|disco|no",
    )
    
    # add features
    parser.add_argument(
        "--add_features",
        default="no",
        type=str,
        help="Which features to add. Options: no|lcf|all|common|lcf+all|lcf+common",
    )


    # only specific languages/corpora
    parser.add_argument(
        "--langs_to_use",
        default="@",
        type=str,
        help="List of languages/corpora to use, a str separated by ;",
    )

    args = parser.parse_args()

    return args
