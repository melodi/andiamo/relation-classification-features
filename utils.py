#!/usr/bin/env python
# coding: utf-8

import os, io
import torch
from transformers import AutoConfig, AutoTokenizer
from configure import parse_args
import numpy as np
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
import matplotlib.pyplot as plt
import seaborn as sns
from time import sleep
from datetime import datetime
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

# open the scikit encoders
label_encoder = LabelEncoder()
onehot_encoder = OneHotEncoder(sparse=False)

now = datetime.now()
dt_string = now.strftime("%d.%m.%y-%H:%M:%S")


args = parse_args()


def open_mappings(mappings_file):
    """Open the mappings file into a dictionary."""

    mappings = {}
    with open(mappings_file, "r") as f:
        next(f)
        for line in f:
            l = line.strip().split("\t")
            mappings[l[0]] = int(l[-1])

    # reject the converted labels
    inv_mappings = {}
    for k, v in mappings.items():
        if v not in inv_mappings:
            inv_mappings[v] = k

    return mappings, inv_mappings


def make_disco_features(list_sentences):
    """Get all the features and make an integer dict."""
    list_features = []
    for sent in list_sentences:
        #         for x in sent[9:34] + sent[35:38]: # all Disco features
        for x in (
            sent[10:17]
            + [sent[19]]
            + sent[22:24]
            + sent[25:29]
            + [sent[30]]
            + [sent[33]]
            + sent[35:38]
        ):  # top Disco features
            list_features.append(x)

    set_features = set(list_features)
    dict_set_features = dict(enumerate(set_features))
    dict_set_features[len(dict_set_features)] = "[UNK]"

    return {v: k for k, v in dict_set_features.items()}


def one_hot_disco_features(raw_features, hot_disco_features):
    """Get all the features and make a one-hot dict."""

    list_features = []
    for sent in list_sentences:
        for x in sent[9:34] + sent[35:38]:
            list_features.append(x)

    set_features = list(set(list_features))
    set_features.append("[UNK]")

    encodings = np.random.uniform(low=0, high=1, size=(len(set_features),))

    dict_set_features = dict(zip(set_features, encodings))

    return dict_set_features


def encode_disco_features(raw_features, hot_disco_features):
    """One-hot encode the feature vector for each sent."""

    encoded = []
    for x in raw_features:
        try:
            encoded.append(hot_disco_features[x])
        except KeyError:
            encoded.append(hot_disco_features["[UNK]"])

    return encoded


def encode_label(og_label, mappings_dict):
    label = og_label.lower().strip()
    if label in mappings_dict:
        return mappings_dict[label]
    else:
        return mappings_dict["unk"]


def open_file(filename, mappings_dict):   
    
    ''' Function to open a .rels file. 
        Arguments: 
        - filename: the path to a .rels file 
        - mappings_dict: a dictionary of mappings of unique labels to integers

        Configure presets (careful!):
        - args.transformer_model
        - args.use_features
        - args.normalize_direction
    '''
    
    lines = []
    if args.transformer_model.startswith('bert'):
        SEP_token = '[SEP]'
        CLS_token = '[CLS]'
    elif args.transformer_model.startswith('roberta'):
        SEP_token = '</s>'
        CLS_token = '<s>'
    else:
        SEP_token = ''
        CLS_token = ''

    langs = {
        "deu": "German",
        "eng": "English",
        "eus": "Basque",
        "fas": "Farsi",
        "fra": "French",
        "ita": "Italian",
        "nld": "Dutch",
        "por": "Portuguese",
        "rus": "Russian",
        "spa": "Spanish",
        "tur": "Turkish",
        "tha": "Thai",
        "zho": "Chinese",
    }

    with open(filename, 'r', encoding='utf-8') as f:
        next(f)
        
        for line in f:
            l = line.strip().split('\t')
            og_label = l[-1]
            # encode the label
            final_label = encode_label(og_label, mappings_dict)

            lang = langs[filename.split("/")[-2].split(".")[0]]
            framework = filename.split("/")[-2].split(".")[1]
            fullname = filename.split("/")[-2]
            
            if len(l) > 1: # empty lines in some files

                # chop the sentences to max_len if too long
                sent_1 = l[3].split(' ')
                sent_2 = l[4].split(' ')      

                if len(sent_1) > max_len:
                    sent_1 = sent_1[:max_len]
                if len(sent_2) > max_len:
                    sent_2 = sent_2[:max_len]
                    
                # add the DiscoDisco features, if we want to
                start_of_seq = [CLS_token]
                if args.use_features == 'lcf':
                    start_of_seq = [lang, framework, fullname] + start_of_seq
                elif args.use_features == 'all':
                    start_of_seq = l[10:34] + l[35:38] + start_of_seq
                elif args.use_features == 'common':
                    start_of_seq = l[10:17]+[l[19]]+l[22:24]+l[25:29]+[l[30]]+[l[33]]+l[35:38] + start_of_seq
                elif args.use_features == 'lcf+all':
                    start_of_seq = [lang, framework, fullname] + l[10:34] + l[35:38] + start_of_seq
                elif args.use_features == 'lcf+common':
                    start_of_seq = [lang, framework, fullname] + l[10:17]+[l[19]]+l[22:24]+l[25:29]+[l[30]]+[l[33]]+l[35:38] + start_of_seq

                # flip them if different direction
                if args.normalize_direction == 'discret':
                    if l[9] == '1>2':
                        lines.append(l + [
                                          start_of_seq + sent_1 + [SEP_token] + sent_2, # sentence in list
                                          final_label]) # encoded labels
                    else:
                        lines.append(l + [
                                          start_of_seq + sent_2 + [SEP_token] + sent_1, 
                                          final_label])

                # implement the Disco direction
                elif args.normalize_direction == 'disco': 
                    if l[9] == '1>2':
                        # it should look like: [CLS] } sent_1 ? > [SEP] sent_2 [SEP]
                        lines.append(l + [ 
                                          start_of_seq + ['}'] + sent_1 + ['>', SEP_token] + sent_2, 
                                          final_label])
                    else:
                        # it should look like: [CLS] sent_1 [SEP] < sent_2 { [SEP]
                        lines.append(l + [ 
                                          start_of_seq + sent_1 + [SEP_token, '<'] + sent_2 + ['{'], 
                                          final_label])  

                # no direction change
                else:
                    lines.append(l + [ 
                                      start_of_seq + sent_1 + [SEP_token] + sent_2, 
                                      final_label])
                
    return lines


def encode_batch(batch):
    """Encodes a batch of input data using the model tokenizer.
    Works for a pandas DF column, instead of a list.
    """
    tokenizer = AutoTokenizer.from_pretrained(args.transformer_model)
    return tokenizer(
        batch["text"], max_length=512, truncation=True, padding="max_length"
    )



# ===============
# OPENING FILES FUNCTIONS
# ===============


def open_sentences_with_feats(path_to_corpora, mappings_dict):
    """Opens all the corpora and the surprise corpora in train/dev/test sets.
    Uses the open_file() function from utils.
    Returns:
    - list of sentences for TRAIN: all the corpora and surprise corpora together
    - dict of sentences for DEV: each dev set categorized per corpus
    - dict of sentences for TEST: each test set categorized per corpus
    """
    langs_to_use = False

    if args.langs_to_use != "@":
        langs_to_use = args.langs_to_use.split(";")

    corpora = [
        folder
        for folder in os.listdir(path_to_corpora)
        if not any(i in folder for i in [".md", "DS_", "utils", "ipynb"])
    ]

    # ---------------------
    train_sentences = []
    dev_dict_sentences = {}
    test_dict_sentences = {}
    disco_features = []
    all_labels = {}

    for corpus in corpora:
        framework = corpus.split(".")[-2]
        if not framework in all_labels:
            all_labels[framework] = []

        # ===== open train ====
        try:
            # open normal files
            if langs_to_use:
                # if we only train with cetrain corpora, we only load them
                train_file = [
                    "/".join([path_to_corpora, corpus, x])
                    for x in os.listdir(path_to_corpora + "/" + corpus)
                    if "train" in x and ".rels" in x
                    if any(l in x for l in langs_to_use)
                ][0]
            else:
                train_file = [
                    "/".join([path_to_corpora, corpus, x])
                    for x in os.listdir(path_to_corpora + "/" + corpus)
                    if "train" in x and ".rels" in x
                ][0]
            temp = open_file(train_file, mappings_dict)
            # add sentence
            train_sentences += temp
            # add disco features
            disco_features += [feature for l in temp for feature in l[10:38]]
            # add labels to framework set
            all_labels[framework] += [l[-1] for l in temp]
        except:  # some of them don't have train
            pass

        # open each test separately
        dev_dict_sentences[corpus] = []
        dev_file = [
            "/".join([path_to_corpora, corpus, x])
            for x in os.listdir(path_to_corpora + "/" + corpus)
            if "dev" in x and ".rels" in x
        ][0]
        temp = open_file(dev_file, mappings_dict)
        dev_dict_sentences[corpus] += temp
        # add disco features
        disco_features += [feature for l in temp for feature in l[10:38]]
        # add labels to framework set
        all_labels[framework] += [l[-1] for l in temp]

        # open each test separately
        test_dict_sentences[corpus] = []
        test_file = [
            "/".join([path_to_corpora, corpus, x])
            for x in os.listdir(path_to_corpora + "/" + corpus)
            if "test" in x and ".rels" in x
        ][0]
        temp = open_file(test_file, mappings_dict)
        test_dict_sentences[corpus] += temp
        # add disco features
        disco_features += [feature for l in temp for feature in l[10:38]]
        # add labels to framework set
        all_labels[framework] += [l[-1] for l in temp]

    corpus_labels = {framework: set(all_labels[framework]) for framework in all_labels}
    # delete unk as a sanity check
    for framework in corpus_labels:
        if "unk" in corpus_labels[framework]:
            corpus_labels[framework].remove("unk")

    disco_features = list(set(disco_features))

    return (
        train_sentences,
        dev_dict_sentences,
        test_dict_sentences,
        corpus_labels,
        disco_features,
    )


# ===============
# Testing functions
# ===============


def get_predictions(model, corpus, test_dataloader, print_results=True):
    """Function to get the model's predictions for one corpus' test set.
    Can print accuracy using scikit-learn.
    Also works with dev sets -- just don't save the outputs.
    Returns: list of predictions that match test file's lines.
    """

    device = torch.device("cuda" if args.use_cuda else "cpu")

    if args.use_cuda:
        model = model.cuda()

    model.eval()
    test_loss, test_accuracy = 0, 0

    all_labels = []
    all_preds = []

    with torch.no_grad():
        for test_input, test_label in test_dataloader:
            mask = test_input["attention_mask"].to(device)
            input_id = test_input["input_ids"].squeeze(1).to(device)
            output = model(input_id, mask)

            logits = output[0]
            logits = logits.detach().cpu().numpy()
            label_ids = test_label.to("cpu").numpy()

            all_labels += label_ids.tolist()
            all_preds += output.argmax(dim=1).tolist()

        assert len(all_labels) == len(all_preds)
        test_acc = round(accuracy_score(all_labels, all_preds), 4)

    if print_results:
        print(corpus, "\tAccuracy:\t", test_acc)

    return all_preds


def get_predictions_huggingface(trainer, corpus, test_set, print_results=True):
    """SPECIFIC FUNCTION FOR THE HUGGINGFACE TRAINER.
    Function to get the model's predictions for one corpus' test set.
    Can print accuracy using scikit-learn.
    Also works with dev sets -- just don't save the outputs.
    Returns: list of predictions that match test file's lines.
    """

    results = trainer.predict(test_set)
    top_preds = np.argmax(results.predictions, axis=1)
    results = results.label_ids
    test_acc = round(accuracy_score(top_preds, results), 4)

    if print_results:
        print(corpus, "\t", test_acc, "\n")

    return top_preds


def better_predictions_huggingface(
    trainer, corpus, test_set, corpus_labels, print_results=True
):
    """ """

    results = trainer.predict(test_set)
    orig_labels = results.label_ids.tolist()
    results_per_sent = results.predictions.tolist()

    # try to make the better prediction bit
    best_labels = []
    for sent, sent_results in enumerate(results_per_sent):
        best_prob = -1000
        best_label = -1

        # assert len(sent_results) == len(orig_labels)

        for n, prob in enumerate(sent_results):
            if n in corpus_labels:
                if prob > best_prob:
                    best_prob = prob
                    best_label = n

        best_labels.append(best_label)

    test_acc = round(accuracy_score(best_labels, orig_labels), 4)
    print("better:\t" + str(test_acc) + "\n", flush="True")

    return best_labels


def make_confusion_matrices(y_test, y_pred, corpus_name, inv_mappings, epoch):
    save_path = "conf_matrix/" + dt_string
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    print(
        classification_report(
            y_test,
            y_pred,
        )
    )

    cm = confusion_matrix(y_test, y_pred, labels=list(inv_mappings.keys()))
    print(cm)

    xticklabels = list(inv_mappings.values())
    yticklabels = list(inv_mappings.values())

    sns.color_palette("cubehelix", as_cmap=True)
    # Plot the confusion matrix.

    fig, ax = plt.subplots()
    #     ax.tick_params(axis='both', which='major', labelsize=6)
    #     ax.tick_params(axis='both', which='minor', labelsize=6)
    ax = sns.heatmap(
        cm,
        # annot=Truex
        xticklabels=xticklabels,
        yticklabels=yticklabels,
    )
    plt.ylabel("Predicted label")
    plt.xlabel("Corpus label")
    plt.xticks(fontsize=2)
    plt.yticks(fontsize=2)
    #     plt.xticks(x, labels, rotation='vertical')
    #     plt.margins(0.5)
    plt.subplots_adjust(bottom=0.5, left=0.5)
    plt.title("Confusion Matrix: " + corpus_name + " (epoch:" + str(epoch) + ")")
    plt.savefig(save_path + "/" + corpus_name + "_" + str(epoch) + ".png", dpi=300)
    plt.clf()


def get_better_predictions(
    model,
    corpus,
    test_dataloader,
    corpus_labels,
    inv_mappings,
    epoch,
    print_results=True,
    save_conf_matrix=False,
):
    device = torch.device("cuda" if args.use_cuda else "cpu")

    if args.use_cuda:
        model = model.cuda()

    model.eval()
    all_labels = []
    all_preds = []

    with torch.no_grad():
        for test_input, test_label in test_dataloader:
            mask = test_input["attention_mask"].to(device)
            input_id = test_input["input_ids"].squeeze(1).to(device)
            output = model(input_id, mask)

            logits = output[0]
            logits = logits.detach().cpu().numpy()
            label_ids = test_label.to("cpu").numpy()

            # all_labels += label_ids.tolist()
            batch_labels = label_ids.tolist()
            batch_probs = []
            for p in output.softmax(dim=-1).tolist():
                batch_probs.append(dict(enumerate(p)))

            for probs in batch_probs:
                final_probs = {}
                sorted_probs = dict(sorted(probs.items(), key=lambda item: item[1]))
                for pred_label in sorted_probs:
                    if pred_label in corpus_labels:
                        final_probs[pred_label] = sorted_probs[pred_label]

                all_preds += [final_probs]

            all_labels += batch_labels

    # get the top predictions in order to get the acc

    top_preds = []
    for probs in all_preds:
        top_preds.append(max(zip(probs.values(), probs.keys()))[1])
    test_acc = round(accuracy_score(all_labels, top_preds), 4)

    if print_results:
        print("better:\t" + str(test_acc), flush="True")

        print(classification_report(all_labels, top_preds))

    if save_conf_matrix:
        make_confusion_matrices(all_labels, top_preds, corpus, inv_mappings, epoch)
    print()
    print("----")

    return all_labels, all_preds
